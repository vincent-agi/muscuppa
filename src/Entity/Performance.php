<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

//! Les performances dans la base de données
/**
 * @ORM\Entity(repositoryClass="App\Repository\PerformanceRepository")
 */
class Performance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id; //!< L'id de la performance, généré autommatiquement

    /**
     * @ORM\Column(type="integer")
     */
    private $nbRepetitionEffectuees; //!< Le nombre de répétitions effectuées

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $dureeEffectuee; //!< La durée effectuée lors de l'exercice

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $niveau; //!< Le niveau auquel la performance a été réalisée (débutant, intermédiaire, expert)

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaire; //!< Un commentaire écrit par l'utilisateur sur sa performance

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exercice", inversedBy="perfomances")
     */
    private $exercice; //!< L'exercice pour lequel la pefromance a été réalisée

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Seance", inversedBy="performances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $seance; //!< La séance durant laquelle la performance a été réalisée

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObjectif(): ?string
    {
        return $this->objectif;
    }

    public function setObjectif(string $objectif): self
    {
        $this->objectif = $objectif;

        return $this;
    }

    public function getNbRepetitionEffectuees(): ?int
    {
        return $this->nbRepetitionEffectuees;
    }

    public function setNbRepetitionEffectuees(int $nbRepetitionEffectuees): self
    {
        $this->nbRepetitionEffectuees = $nbRepetitionEffectuees;

        return $this;
    }

    public function getDureeEffectuee(): ?string
    {
        return $this->dureeEffectuee;
    }

    public function setDureeEffectuee(string $dureeEffectuee): self
    {
        $this->dureeEffectuee = $dureeEffectuee;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getExercice(): ?Exercice
    {
        return $this->exercice;
    }

    public function setExercice(?Exercice $exercice): self
    {
        $this->exercice = $exercice;

        return $this;
    }

    public function getSeance(): ?Seance
    {
        return $this->seance;
    }

    public function setSeance(?Seance $seance): self
    {
        $this->seance = $seance;

        return $this;
    }

    public function __toString()
    {
      return $this->id;
    }
}
