<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

//! Les aiments dans la base de données
/**
 * @ORM\Entity(repositoryClass="App\Repository\AlimentRepository")
 */
class Aliment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id; //!< L'id de l'aliment, générée automatiquement

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom; //!< Le nom de l'aliment

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $kcal; //!< La quantité de kilocalorien contenues dans l'aliment

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $glucides; //!< La quantité de glucides dans l'aliment

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $protides; //!< La quantité de protides (protéines) dans l'aliment

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lipides; //!< La quantité de lipides dans l'aliment

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type; //!< Le type de l'aliment (féculent, légumineuse, etc.)

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getKcal(): ?string
    {
        return $this->kcal;
    }

    public function setKcal(?string $kcal): self
    {
        $this->kcal = $kcal;

        return $this;
    }

    public function getGlucides(): ?string
    {
        return $this->glucides;
    }

    public function setGlucides(?string $glucides): self
    {
        $this->glucides = $glucides;

        return $this;
    }

    public function getProtides(): ?string
    {
        return $this->protides;
    }

    public function setProtides(?string $protides): self
    {
        $this->protides = $protides;

        return $this;
    }

    public function getLipides(): ?string
    {
        return $this->lipides;
    }

    public function setLipides(?string $lipides): self
    {
        $this->lipides = $lipides;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function __toString()
    {
      return $this->nom;
    }
}
