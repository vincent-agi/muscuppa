<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

//! Les séances dans la base de données
/**
 * @ORM\Entity(repositoryClass="App\Repository\SeanceRepository")
 */
class Seance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id; //!< L'id de la séance, généré automatiquement

    /**
     * @ORM\Column(type="datetime")
     */
    private $date; //!< La date à laquelle la séance à été réalisée

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="seances")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user; //! L'utilisateur qui a fait la séance

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Performance", mappedBy="seance", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $performances;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre; //!< Les performances réalisées lors de la séance

    public function __construct()
    {
        $this->performances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Performance[]
     */
    public function getPerformances(): Collection
    {
        return $this->performances;
    }

    public function addPerformance(Performance $performance): self
    {
        if (!$this->performances->contains($performance)) {
            $this->performances[] = $performance;
            $performance->setSeance($this);
        }

        return $this;
    }

    public function removePerformance(Performance $performance): self
    {
        if ($this->performances->contains($performance)) {
            $this->performances->removeElement($performance);
            // set the owning side to null (unless already changed)
            if ($performance->getSeance() === $this) {
                $performance->setSeance(null);
            }
        }

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $date = new \Datetime('now');
        $date = $date->format("d-m-Y H:i");
        $this->titre = $titre.'-'.$date;

        return $this;
    }

    public function __toString()
    {
      return $this->getTitre();
    }
}
