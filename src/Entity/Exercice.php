<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

//! Les exercices dans la base de données
/**
 * @ORM\Entity(repositoryClass="App\Repository\ExerciceRepository")
 */
class Exercice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id; //!< L'id de l'exercice, généré automatiquement

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom; //!< Le nom de l'exercice

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAjout; //!< La date à lquelle l'exercice a été aajouté dans la base de données

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lienVideo; //!< Le lien vers le tutoriel vidéo de l'exercice

    /**
     * @ORM\Column(type="json")
     */
    private $zone; //!< La zone musculaire travaillée par l'exercice

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $charge; //!< La charge à soulever/pousser/tirer

    /**
     * @ORM\Column(type="integer")
     */
    private $repMax; //!< La répétition max d'un utilisateur à cet exercice

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $dureeMax; //!< La durée maximale de l'exercice (dans le case d'un exercice de durée, la planche par exemple)

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="exercices")
     */
    private $users; //!< Les utilisateurs liée à l'exercice (utile pour la répétition max et les performances)

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Performance", mappedBy="exercice", cascade={"persist", "remove"})
     */
    private $perfomances; //!< Les performances d'un utilisateur à cet exercice

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $coef; //!< Le coeficient à appliquer à la formule pour calculer, la charge, le nombre de répétitions et la durée

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->perfomances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDateAjout(): ?\DateTimeInterface
    {
        return $this->dateAjout;
    }

    public function setDateAjout(\DateTimeInterface $dateAjout): self
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    public function getLienVideo(): ?string
    {
        return $this->lienVideo;
    }

    public function setLienVideo(?string $lienVideo): self
    {
        $this->lienVideo = $lienVideo;

        return $this;
    }

    public function getZone(): ?array
    {
        return $this->zone;
    }

    public function setZone(array $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

    public function getCharge(): ?float
    {
        return $this->charge;
    }

    public function setCharge(?float $charge): self
    {
        $this->charge = $charge;

        return $this;
    }

    public function getRepMax(): ?int
    {
        return $this->repMax;
    }

    public function setRepMax(int $repMax): self
    {
        $this->repMax = $repMax;

        return $this;
    }

    public function getDureeMax(): ?string
    {
        return $this->dureeMax;
    }

    public function setDureeMax(string $dureeMax): self
    {
        $this->dureeMax = $dureeMax;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addExercice($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeExercice($this);
        }

        return $this;
    }

    /**
     * @return Collection|Performance[]
     */
    public function getPerfomances(): Collection
    {
        return $this->perfomances;
    }

    public function addPerfomance(Performance $perfomance): self
    {
        if (!$this->perfomances->contains($perfomance)) {
            $this->perfomances[] = $perfomance;
            $perfomance->setExercice($this);
        }

        return $this;
    }

    public function removePerfomance(Performance $perfomance): self
    {
        if ($this->perfomances->contains($perfomance)) {
            $this->perfomances->removeElement($perfomance);
            // set the owning side to null (unless already changed)
            if ($perfomance->getExercice() === $this) {
                $perfomance->setExercice(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
      return $this->getNom();
    }

    public function getCoef(): ?float
    {
        return $this->coef;
    }

    public function setCoef(?float $coef): self
    {
        $this->coef = $coef;

        return $this;
    }
}
