<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

//! L'adresse dans la base de données
/**
 * @ORM\Entity(repositoryClass="App\Repository\AdresseRepository")
 */
class Adresse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id; //!< L'id de l'adresse, générée automatiquement

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numRue; //!< Le numéro de rue

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ville; //!< La ville

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codePostal; //!< Le code postal de la ville

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomRue; //!< Le nom de la rue

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="adresse")
     */
    private $users; //!< Le(s) utilisateur(s) habitant à cette adresse

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumRue(): ?int
    {
        return $this->numRue;
    }

    public function setNumRue(?int $numRue): self
    {
        $this->numRue = $numRue;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(?int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getNomRue(): ?string
    {
        return $this->nomRue;
    }

    public function setNomRue(?string $nomRue): self
    {
        $this->nomRue = $nomRue;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setAdresse($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getAdresse() === $this) {
                $user->setAdresse(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
      return $this->numRue . " " . $this->nomRue . " " . $this->ville;
    }
}
