<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//! Les utilisateurs en base de dnnées
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id; //!< L'id de l'utilisateur, généré automatiquement

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email; //!< L'adresse email de l'utilisateur

    /**
     * @ORM\Column(type="json")
     */
    private $roles = []; //!< Le ou les rôles de l'utilisateur (utilisateur ou administrateur)

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password; //!< Le mot de passe de l'utilisateur

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom; //!< Le nom de l'utilisateur

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom; //!< Le prénom de l'utilisateur

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateInscription; //!< La date d'inscription de l'utilisateur

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateConnexion; //!< La date de dernière connexion de l'utilisateur

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $niveau; //!< Le niveau de l'utilisateur (débutant, intermédiaire, expert)

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Adresse", inversedBy="users", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $adresse; //!< L'adresse de l'utiilisateur

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Seance", mappedBy="user", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $seances; //!< Les séances que l'utilisateur à réalisées

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Exercice", inversedBy="users", cascade={"persist", "remove"})
     */
    private $exercices; //!< Les exercices, pour connaître le niveua de l'utilisateur à ceux-ci

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive; //!< Si l'utilisateur est actif

    // Seciruty Encoder
    private $passwordEncoder; //!< Pour le hachage du mot de passe

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $sexe; //!< Le genre de 'utilisateur'

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $objectif; //!< L'objectif de l'utilisateur (hypertrophie, gain de force)

    public function __construct()
    {
        $this->seances = new ArrayCollection();
        $this->exercices = new ArrayCollection();
        $this->isActive = true;
    }

    public function getPasswordEncoder()
    {
      return $this->passwordEncoder;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        return "SortCut*2020";
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateInscription(): ?\DateTimeInterface
    {
        return $this->dateInscription;
    }

    public function setDateInscription(?\DateTimeInterface $dateInscription): self
    {
        $this->dateInscription = $dateInscription;

        return $this;
    }

    public function getDateConnexion(): ?\DateTimeInterface
    {
        return $this->dateConnexion;
    }

    public function setDateConnexion(?\DateTimeInterface $dateConnexion): self
    {
        $this->dateConnexion = $dateConnexion;

        return $this;
    }

    public function getNiveau(): ?string
    {
        return $this->niveau;
    }

    public function setNiveau(string $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|Seance[]
     */
    public function getSeances(): Collection
    {
        return $this->seances;
    }

    public function addSeance(Seance $seance): self
    {
        if (!$this->seances->contains($seance)) {
            $this->seances[] = $seance;
            $seance->setUser($this);
        }

        return $this;
    }

    public function removeSeance(Seance $seance): self
    {
        if ($this->seances->contains($seance)) {
            $this->seances->removeElement($seance);
            // set the owning side to null (unless already changed)
            if ($seance->getUser() === $this) {
                $seance->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exercice[]
     */
    public function getExercices(): Collection
    {
        return $this->exercices;
    }

    public function addExercice(Exercice $exercice): self
    {
        if (!$this->exercices->contains($exercice)) {
            $this->exercices[] = $exercice;
        }

        return $this;
    }

    public function removeExercice(Exercice $exercice): self
    {
        if ($this->exercices->contains($exercice)) {
            $this->exercices->removeElement($exercice);
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function __toString()
    {
      return $this->email;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getObjectif(): ?string
    {
        return $this->objectif;
    }

    public function setObjectif(string $objectif): self
    {
        $this->objectif = $objectif;

        return $this;
    }
}
