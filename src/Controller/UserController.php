<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//! Le controlleur de gestion des utilisateurs
/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    //! La méthode permettant de voir la liste des utilisateurs
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant d'ajouter un utilisateur
    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          // password in plain text
            $passwordPlainText = $form->get('password')->getData();
            // encode password with algorithm was define in security.yaml
            $user->setPassword($encoder->encodePassword($user, $passwordPlainText));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant de voir les détails d'un utilisateur
    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function show(User $user): Response
    {
        $this->denyAccessUnlessGranted('VIEW', $user);
        return $this->render('user/show.html.twig', [
            'user' => $user,
            'utilisateur'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant à un utilisateur de modifier ses caractéristiques
    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $encoder): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $user);
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $passwordPlainText = $form->get('password')->getData();
            if ($passwordPlainText == null) {
              throw new \Exception("password null", 1);

            }
            $user->setPassword($encoder->encodePassword($user, $passwordPlainText));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('accueil');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'utilisateur'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant à l'administrateur de modifier les caractéristiques d'un utilisateur
    /**
     * @Route("/admin/{id}/edit", name="user_edit_admin", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit_admin(Request $request, User $user): Response
    {
        $form = $this->createForm(UserAdminType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    //! La méthode permettant de supprimer un utilisateur
    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, User $user): Response
    {
        $this->denyAccessUnlessGranted('DELETE', $user);
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
