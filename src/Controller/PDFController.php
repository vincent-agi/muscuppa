<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use FPDF;

class PDFController extends AbstractController
{
    /**
     * @Route("/fiche", name="fiche")
     */
    public function fiche()
    {
      $user = $this->getUser();
      $seances = $user->getSeances();
      require_once('../FPDF/fpdf.php');
      $pdf = new FPDF();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
      $pdf->MultiCell(0, 10, "Fiche des performances de ".$user->getNom()." ".$user->getPrenom(), 1, 'C');
      $pdf->Ln();
      $pdf->Image('./images/uppa.png', null, null, 190, 70);
      $pdf->AddPage();
      $pdf->SetFontSize(11);
      foreach ($seances as $seance) {
        foreach ($seance->getPerformances() as $performance) {
          $exercice = $performance->getExercice();
          $pdf->MultiCell(0, 10, utf8_decode("Nom de l'exercice : ".$exercice->getNom()), 0, 'C');
          $pdf->MultiCell(0, 10, utf8_decode("Zones travaillées : ". utf8_decode(implode(',', $exercice->getZone()))), 0);
          $pdf->MultiCell(0, 10, utf8_decode("Nom de la séance : ".$seance->getTitre()), 0);
          $pdf->MultiCell(0, 10, utf8_decode("Niveau : ".$performance->getNiveau()), 0);
          $pdf->MultiCell(0, 10, utf8_decode("Nombre de répétition : ".$performance->getNbRepetitionEffectuees()), 0);
          $pdf->MultiCell(0, 10, utf8_decode("Commnentaire de la séance : ".$performance->getCommentaire()), 0);
          $pdf->Ln();
          $pdf->Ln();
        }
      }
      return new Response($pdf->Output(), 200, array('Content-Type' => 'application/pdf'));
    }
}
