<?php

namespace App\Controller;

use App\Entity\Performance;
use App\Entity\Exercice;
use App\Form\PerformanceType;
use App\Repository\PerformanceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

//! Le controlleur responsable de la gestion des performances de l'utilisateur
/**
 * @Route("/performance")
 */
class PerformanceController extends AbstractController
{
    //! La méthode permettant d'afficher les performances de l'utilisateur
    /**
     * @Route("/", name="performance_index", methods={"GET"})
     */
    public function index(PerformanceRepository $performanceRepository): Response
    {
        $exercices = [];
        foreach ($performanceRepository->findPerformanceByUser($this->getUser()->getId()) as $performance) {
            array_push($exercices, $performance->getExercice());
        }

        return $this->render('performance/index.html.twig', [
            'performances' => $performanceRepository->findPerformanceByUser($this->getUser()->getId()), // revoir la requete DQL findPerformanceByUser
            'user'=>$this->getUser(),
            'exercices' => array_unique($exercices),
        ]);
    }

    //! La méthode permettant d'ajouter une performance dans la base
    /**
     * @Route("/new", name="performance_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $performance = new Performance();
        $form = $this->createForm(PerformanceType::class, $performance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($performance);
            $entityManager->flush();

            return $this->redirectToRoute('performance_index');
        }

        return $this->render('performance/new.html.twig', [
            'performance' => $performance,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant de voir les détails d'une performance
    /**
     * @Route("/{id}", name="performance_show", methods={"GET"})
     */
    public function show(Exercice $exercice): Response
    {
        // $this->denyAccessUnlessGranted('VIEW', $performances);
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT p FROM App\Entity\Performance p JOIN p.seance s WHERE p.exercice = :exercice ORDER BY s.date ASC');
        $query->setParameter('exercice', $exercice);
        $performances = $query->getResult();

        return $this->render('performance/show.html.twig', [
            'exercice' => $exercice,
            'performances' => $performances,
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant de modifier les caractéristiques d'une performance
    /**
     * @Route("/{id}/edit", name="performance_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Performance $performance): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $performance);
        $form = $this->createForm(PerformanceType::class, $performance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('performance_index');
        }

        return $this->render('performance/edit.html.twig', [
            'performance' => $performance,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! la méthode permettant de supprimer une performance de la base
    /**
     * @Route("/{id}", name="performance_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Performance $performance): Response
    {
        $this->denyAccessUnlessGranted('DELETE', $performance);
        if ($this->isCsrfTokenValid('delete'.$performance->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($performance);
            $entityManager->flush();
        }

        return $this->redirectToRoute('performance_index');
    }
}
