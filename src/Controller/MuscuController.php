<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormView;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Entity\User;
use App\Entity\Aliment;
use App\Entity\Exercice;
use App\Repository\UserRepository;

//! Le contrôleur principal, responsable de la page d'accueil.
class MuscuController extends AbstractController
{
    //! La méthode de la page d'accueil
    /**
    * @Route("/profile", name="accueil")
    */
    public function index()
    {
        $user = $this->getUser();
        $user->setDateConnexion(new \Datetime());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        $nbExercices = count($this->getDoctrine()->getRepository(Exercice::class)->findAll());
        $nbAliments = count($this->getDoctrine()->getRepository(Aliment::class)->findAll());
        return $this->render('muscu/index.html.twig', [
            'user' => $user,
            'nbExercices' => $nbExercices,
            'nbAliments' => $nbAliments,
        ]);
    }

    //! La méthode permettant d'accéder au formulaire de contact
    /**
    * @Route("/contact", name="contact")
    */
    public function contact(Request $request, \Swift_Mailer $mailer): Response
    {
        $user = $this->getUser();
        if (!$user) {
            throw new \Exception('Utilisateur non identifié');
        }

        $form = $this->createFormBuilder()
        ->add('raison', ChoiceType::class, [
            'label' => 'Pour quelle raison nous contactez vous ?',
            'required' => true,
            'choices' => [
                'Probléme technique (compte bloqué, ressource indisponible, ...)' => 'Probléme technique',
                'Suggestion d\'amélioration' => 'Suggestion',
                'Question diverse' => 'Question diverse',
                'Autre' => 'Autre',
            ],
        ])
        ->add('titre', TextType::class, [
            'label' => 'Titre',
            'required'=>true,
        ])
        ->add('message', TextareaType::class, [
            'label' => 'Message',
            'required'=>true,
            'attr' => [
                'placeholder' => 'Décrivez votre demande.'
            ],
        ])
        ->add('envoyer', SubmitType::class, [
            'label'=> 'Envoyer',
        ])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailUser = $this->getUser()->getEmail();
            $userName = $this->getUser()->getNom() . " ". $this->getUser()->getPrenom();

            $raison = $form->Get('raison')->getData();
            switch ($raison) {
                case 'Probléme technique':
                // contacte l'équipe chargé du support technique.
                $emailTo = 'agi.v@hotmail.fr';
                break;
                case 'Suggestion':
                // contacte l'équipe chargée de gérer les suggestions pour faire évoluer l'application.
                $emailTo = 'doccataplare@gmail.com';
                break;
                case 'Question diverse':
                // Contacte l'équipe chargée de traiter les questions diverses
                $emailTo = 'tomas.lapeyre.bady@free.fr';
                break;
                default:
                // traitement par défaut des mails si aucune des raisons si dessus n'est prise en compte.
                $emailTo ='p.alborghetti@outlook.fr';
                break;
            }
            $title = $form->get('titre')->getData();
            $body = $form->get('message')->getData();
            $message = (new \Swift_Message($title))
            ->setFrom($emailUser)
            ->setTo($emailTo)
            ->setBody("$body
            <br><br>
            De : $userName. <br> Veuillez me répondre sur $emailUser <br><br>
            Mail Envoyé avec l'application MuscuPPA de l'IUT de Baysonne et du Paus Basque.",
            'text/html'
        );

        $mailer->send($message);

        //message de confirmation de l'envoi
        return $this->render('muscu/confirmationEnvoi.html.twig', [
            'user'=>$this->getUser(),
        ]);

    }

    return $this->render('muscu/contact.html.twig', [
        'form' => $form->createView(),
        'user'=>$this->getUser(),
    ]);
}
}
