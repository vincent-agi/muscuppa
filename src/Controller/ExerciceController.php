<?php

namespace App\Controller;

use App\Entity\Exercice;
use App\Form\ExerciceType;
use App\Repository\ExerciceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//! Le controlleur de gestion des exercices
/**
 * @Route("/exercice")
 */
class ExerciceController extends AbstractController
{
    //! La méthode permettant d'afficher la liste des exercices
    /**
     * @Route("/", name="exercice_index", methods={"GET"})
     */
    public function index(ExerciceRepository $exerciceRepository): Response
    {
        return $this->render('exercice/index.html.twig', [
            'exercices' => $exerciceRepository->findAll(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant d'ajouter un exercice dans la base
    /**
     * @Route("/new", name="exercice_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $exercice = new Exercice();
        $form = $this->createForm(ExerciceType::class, $exercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exercice->setDateAjout(new \Datetime('now'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($exercice);
            $entityManager->flush();

            return $this->redirectToRoute('exercice_index');
        }

        return $this->render('exercice/new.html.twig', [
            'exercice' => $exercice,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant de voir les détailes d'un exercice, et de l'effectuer
    /**
     * @Route("/{id}", name="exercice_show", methods={"GET"})
     */
    public function show(Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('VIEW', $exercice);
        return $this->render('exercice/show.html.twig', [
            'exercice' => $exercice,
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant de modifier les caractéristiques d'un exercice dans la base
    /**
     * @Route("/{id}/edit", name="exercice_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, Exercice $exercice): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $exercice);
        $form = $this->createForm(ExerciceType::class, $exercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('exercice_index');
        }

        return $this->render('exercice/edit.html.twig', [
            'exercice' => $exercice,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant de supprimer un exercice
    /**
     * @Route("/{id}", name="exercice_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Exercice $exercice): Response
    {
        if ($this->isCsrfTokenValid('delete'.$exercice->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($exercice);
            $entityManager->flush();
        }

        return $this->redirectToRoute('exercice_index');
    }
}
