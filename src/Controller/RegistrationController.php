<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Adresse;
use App\Form\RegistrationFormType;
use App\Security\MuscuppaAuthenticator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

//! Le controlleur responsable de l'ajout d'un utilisateur dans la base
class RegistrationController extends AbstractController
{
    //! La méthode permettant d'ajouter un utilisateur
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, MuscuppaAuthenticator $authenticator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $user->setDateInscription(new \Datetime('now'));
            /*
            $adresse = new Adresse();
            $adresse->setNomRue($form->get('nomRue')-getData());
            $adresse->setVille()$form->get('ville')-getData());
            $adresse->setNumRue()$form->get('numRue')-getData());
            $adresse->setCodePostal($form->get('codePostal')-getData());
            $adresse->setNumRue($form->get('nomRue')-getData());
            $user->setAdresse($adresse);
            */
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
