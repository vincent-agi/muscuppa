<?php

namespace App\Controller;

use App\Entity\Seance;
use App\Form\SeanceType;
use App\Repository\SeanceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

//! Le controleur permettant de gérer les séances
/**
 * @Route("/seance")
 */
class SeanceController extends AbstractController
{
    //! La méthode permettant de voir la liste des séances
    /**
     * @Route("/", name="seance_index", methods={"GET"})
     */
    public function index(SeanceRepository $seanceRepository): Response
    {
        return $this->render('seance/index.html.twig', [
            'seances' => $seanceRepository->findSeanceByUser($this->getUser()),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant d'ajouter une séance
    /**
     * @Route("/new", name="seance_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $seance = new Seance();
        $user = $this->getUser();
        $form = $this->createForm(SeanceType::class, $seance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          $titre = $form->get('titre')->getData();
          $seance->setTitre($titre);
          $seance->setDate(new \Datetime('now'));
          $seance->setUser($user);
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($seance);
          $entityManager->flush();

          return $this->redirectToRoute('seance_index');
        }
        return $this->render('seance/new.html.twig', [
            'seance' => $seance,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthoe permettant de voir les détais d'une séance
    /**
     * @Route("/{id}", name="seance_show", methods={"GET"})
     */
    public function show(Seance $seance): Response
    {
        $this->denyAccessUnlessGranted('VIEW', $seance);
        return $this->render('seance/show.html.twig', [
            'seance' => $seance,
            'user'=>$this->getUser(),
        ]);
    }

    //! La méthode permettant d'éditer les détails d'une séance
    /**
     * @Route("/{id}/edit", name="seance_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Seance $seance): Response
    {
        $this->denyAccessUnlessGranted('EDIT', $seance);
        $form = $this->createForm(SeanceType::class, $seance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('seance_index');
        }

        return $this->render('seance/edit.html.twig', [
            'seance' => $seance,
            'form' => $form->createView(),
            'user'=>$this->getUser(),
        ]);
    }

    //! La route permettant de supprimmer une séance
    /**
     * @Route("/{id}", name="seance_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Seance $seance): Response
    {
        $this->denyAccessUnlessGranted('DELETE', $exercice);
        if ($this->isCsrfTokenValid('delete'.$seance->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($seance);
            $entityManager->flush();
        }

        return $this->redirectToRoute('seance_index');
    }
}
