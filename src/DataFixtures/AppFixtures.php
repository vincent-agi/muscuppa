<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Seance;
use App\Entity\Performance;
use App\Entity\Exercice;
use App\Entity\Aliment;
use App\Entity\Adresse;
use Faker;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        //vincent user : user and admin
        $vincent = new User();
        $vincent->setEmail('agi.v@hotmail.fr');
        $vincent->setRoles(["ROLE_USER","ROLE_ADMIN"]);
        //password  : azerty
        $vincent->setPassword('$2y$13$wGsnMD.XwgvblzK1I2N8kObBv54s/.biqqmm/5RI2YKqt3E8Nemg2');
        $vincent->setNom('AGI');
        $vincent->setPrenom('Vincent');
        $vincent->setDateInscription(new \Datetime('now'));
        $vincent->setNiveau('Débutant');
        $vincent->setSexe('Homme');
        $vincent->setObjectif('Hypertrophie');

        $pierre = new User();
        $pierre->setEmail('alborghetti.pierre.4e2gp1@gmail.com');
        $pierre->setRoles(["ROLE_USER","ROLE_ADMIN"]);
        //password azerty
        $pierre->setPassword('$2y$13$gVqERRX3eHKiw4vITgyFmuZVlr6m9W2Eq9fZFyv4lIVaAMKYvJnvK');
        $pierre->setNom('ALBORGHETTI');
        $pierre->setPrenom('Pierre');
        $pierre->setDateInscription(new \Datetime('now'));
        $pierre->setNiveau('Intermédiaire');
        $pierre->setSexe('Homme');
        $pierre->setObjectif('Hypertrophie');

        $thomas = new User();
        $thomas->setEmail('toma.lapeyre@gmail.com');
        $thomas->setRoles(["ROLE_USER","ROLE_ADMIN"]);
        //password azerty
        $thomas->setPassword('$2y$13$l5cgFMZDrZ/kYBHdznBxuO1KO7TTN3mpeZPSpT8X6umRVPalLlrce');
        $thomas->setNom('LAPEYRE');
        $thomas->setPrenom('Thomas');
        $thomas->setDateInscription(new \Datetime('now'));
        $thomas->setNiveau('Intermédiaire');
        $thomas->setSexe('Homme');
        $thomas->setObjectif('Gain de force');
/*
        $dubertrand = new User();
        $dubertrand->setEmail('herve.dubertrand@univ-pau.fr');
        $dubertrand->setRoles(["ROLE_USER","ROLE_ADMIN"]);
        //password azerty
        $dubertrand->setPassword('$2y$13$tWAm9lKWXlluSfiA2jI/8Ovo5IU9Yg17x.SKwAyMLSee3BazJ5eMq');
        $dubertrand->setNom('DUBERTRAND');
        $dubertrand->setPrenom('Hervé');
        $dubertrand->setDateInscription(new \Datetime('now'));
        $dubertrand->setNiveau('Expert');*/

        $manager->persist($vincent);
        $manager->persist($thomas);
        $manager->persist($pierre);
        //$manager->persist($dubertrand);
        $manager->flush();

         $Faker = Faker\Factory::create('fr_FR');

        for ($i = 0 ; $i < 25 ; $i++)
        {
          $user = new User();
          $user->setEmail($Faker->email);
          $user->setRoles(["ROLE_USER"]);
          $user->setPassword($Faker->password);
          $user->setNom($Faker->lastName);
          $user->setPrenom($Faker->firstName);
          $user->setDateInscription($Faker->datetime($max = 'now'));
          $niveau = $Faker->randomElements($array = array ('Debutant', 'Intermediaire', 'Expert'));
          $user->setNiveau($niveau[0]);
          $sexe=$Faker->randomElements($array = array ('Homme', 'Femme', 'Autre' , 'non renseigné'), $count = 1);
          $user->setSexe($sexe[0]);
          $user->setObjectif('Hypertrophie');

          $adresse = new Adresse();
          $adresse->setNumRue($Faker->randomDigit);
          $adresse->setVille($Faker->city);
          $adresse->setCodePostal(intval($Faker->postcode));
          $adresse->setNomRue($Faker->streetName);

          $user->setAdresse($adresse);

          $seance1 = new Seance();
          $seance2 = new Seance();
          $seance3 = new Seance();
          $seance4 = new Seance();
          $seance1->setDate($Faker->datetimeBetween($startDate = '- 2 years', $endDate = 'now'));
          $seance2->setDate($Faker->datetimeBetween($startDate = '- 2 years', $endDate = 'now'));
          $seance3->setDate($Faker->datetimeBetween($startDate = '- 2 years', $endDate = 'now'));
          $seance4->setDate($Faker->datetimeBetween($startDate = '- 2 years', $endDate = 'now'));
          $seance1->setTitre($Faker->realText($maxNbChars = 15, $indexSize = 2));
          $seance2->setTitre($Faker->realText($maxNbChars = 15, $indexSize = 2));
          $seance3->setTitre($Faker->realText($maxNbChars = 15, $indexSize = 2));
          $seance4->setTitre($Faker->realText($maxNbChars = 15, $indexSize = 2));

          $performance1 = new Performance();
          $performance2 = new Performance();
          $performance3 = new Performance();
          $performance4 = new Performance();
          $performance1->setNbRepetitionEffectuees($Faker->randomDigit);
          $performance2->setNbRepetitionEffectuees($Faker->randomDigit);
          $performance3->setNbRepetitionEffectuees($Faker->randomDigit);
          $performance4->setNbRepetitionEffectuees($Faker->randomDigit);
          $performance1->setDureeEffectuee($Faker->randomNumber);
          $performance2->setDureeEffectuee($Faker->randomNumber);
          $performance3->setDureeEffectuee($Faker->randomNumber);
          $performance4->setDureeEffectuee($Faker->randomNumber);
          $niveau = $Faker->randomElements($array = array ('Debutant', 'Intermediaire', 'Expert'));
          $performance1->setNiveau($niveau[0]);
          $performance2->setNiveau($niveau[0]);
          $performance3->setNiveau($niveau[0]);
          $performance4->setNiveau($niveau[0]);
          $performance1->setCommentaire($Faker->text);
          $performance2->setCommentaire($Faker->text);
          $performance3->setCommentaire($Faker->text);
          $performance4->setCommentaire($Faker->text);

          $exercice = new Exercice();
          $nom = $Faker->randomElements($array = array('Pompes', 'Traction' , 'Burpee', 'Crunch', 'Jogging'));
          $nom = $nom[0].$Faker->randomDigit;
          $exercice->setNom($nom);
          $exercice->setDateAjout($Faker->datetimeBetween($startDate = '- 2 years', $endDate = 'now'));
          $exercice->setLienVideo($Faker->url);
          $zone = $Faker->randomElements($array = array ('Avants bras' , 'Bras' , 'Dos' , 'Ceinture abdominale', 'Cuisses', 'Mollets' ), $count=3);
          $exercice->setzone($zone);
          $exercice->setCharge($Faker->randomFloat);
          $exercice->setRepMax($Faker->randomDigit);
          $exercice->setDureeMax($Faker->randomDigit);

          $aliment = new Aliment();
          $nom = ($Faker->randomElements($array = array('Pain', 'Poulet' , 'Riz', 'Légume', 'Céréale')));
          $nom = $nom[0].$Faker->randomDigit;
          $aliment->setNom($nom);
          $aliment->setKcal($Faker->randomDigit);
          $aliment->setGlucides($Faker->randomDigit);
          $aliment->setProtides($Faker->randomDigit);
          $aliment->setLipides($Faker->randomDigit);
          $type = $Faker->randomElements($array = array('Viande', 'Féculent' , 'Viande', 'Légumineuse', 'Crudité'));
          $aliment->setType($type[0]);

          $manager->persist($aliment);




          $performance1->setExercice($exercice);
          $performance2->setExercice($exercice);
          $performance3->setExercice($exercice);
          $performance4->setExercice($exercice);

          $seance1->addPerformance($performance1);
          $seance2->addPerformance($performance2);
          $seance3->addPerformance($performance3);
          $seance4->addPerformance($performance4);

          $thomas->addSeance($seance1);
          $thomas->addSeance($seance2);
          $thomas->addSeance($seance3);
          $thomas->addSeance($seance4);
          $thomas->addExercice($exercice);

          $manager->persist($user);
          $manager->persist($adresse);
          $manager->persist($seance1);
          $manager->persist($seance2);
          $manager->persist($seance3);
          $manager->persist($seance4);
          $manager->persist($performance1);
          $manager->persist($performance2);
          $manager->persist($performance3);
          $manager->persist($performance4);
          $manager->persist($exercice);
          $manager->flush();
        }






    }
}
