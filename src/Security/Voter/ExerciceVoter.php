<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class ExerciceVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['EDIT', 'VIEW', 'DELETE'])
            && $subject instanceof \App\Entity\Exercice;
    }

    protected function voteOnAttribute($attribute, $exercice, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'EDIT':
                return $this->security->isGranted('ROLE_ADMIN');
                break;
            case 'VIEW':
                if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_USER')) {
                  return true;
                }else {
                  return false;
                }
                break;
            case 'DELETE':
                return $this->security->isGranted('ROLE_ADMIN');
              break;
        }

        return false;
    }
}
