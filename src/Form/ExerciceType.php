<?php

namespace App\Form;

use App\Entity\Exercice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ExerciceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
              'label'=> 'Nom de l\'exercice',
            ])
            ->add('zone', ChoiceType::class, [
              'label'=>'Quelles sont les zones travaillées ?',
              'required'=> true,
              'multiple'=>true,
              'expanded'=>true,
              'choices'=> [
                'Avants bras'=> 'Avants bras',
                'Bras'=>'Bras',
                'Dos'=> 'Dos',
                'Ceinture abdominale'=>'abdos',
                'Cuisses'=>'Cuisses',
                'Mollets'=>'Mollets',
              ],
            ])
            ->add('charge', TextType::class, [
              'label'=>'Quelle est la charge maximale conseillée (Utile si le sexe de l\'utilisateur n\'est pas renseigné) ?',
            ])
            ->add('coef', TextType::class, [
              'label'=>'Quel est le coefficeint de charge si du poids intervient durant l\'exercice ?',
            ])
            ->add('repMax', TextType::class, [
              'label'=> 'Quelle est la répétition maximale conseillée ?'
            ])
            ->add('dureeMax')
            ->add('lienVideo')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Exercice::class,
        ]);
    }
}
