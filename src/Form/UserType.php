<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Adresse;
use App\Form\AdresseType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('password', PasswordType::class, [
              'attr'=> [
                'placeholder' => '',
              ],
              'required'=> true,
            ])
            ->add('nom')
            ->add('prenom')
            ->add('niveau', ChoiceType::class, [
              'choices'=>[
                'Débutant'=>'Débutant',
                'Intermédiaire' => 'Intermédiaire',
                'Expert'=>'Expert',
              ]
            ])
            ->add('objectif', ChoiceType::class, [
              'label'=> 'Quel est votre objectif ?',
              'choices'=> [
                'Hypertrophie'=> 'Hypertrophie',
                'Gain de force'=>'Force',
              ],
            ])
            ->add('sexe', ChoiceType::class, [
              'label'=>'Quel est votre sexe ?',
              'choices'=> [
                'Homme'=>'Homme',
                'Femme'=> 'Femme',
                'Autre'=>'Autre',
                'Ne souhaite pas donner cette information'=> 'non renseigné',
              ],
            ])
            ->add('adresse', AdresseType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
