<?php

namespace App\Form;

use App\Entity\Performance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Exercice;
use App\Entity\Seance;
use Doctrine\ORM\EntityRepository;


class PerformanceType extends AbstractType
{
  private $tokenStorage;

public function __construct(TokenStorageInterface $tokenStorage)
{
  $this->tokenStorage = $tokenStorage;
}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbRepetitionEffectuees', TextType::class, [
              'label'=>'Combien avez vous fait de répétitions ?',
            ])
            ->add('dureeEffectuee', TextType::class, [
              'label'=>'Quelle a été la durée de votre exercice ?',
            ])
            ->add('niveau', ChoiceType::class, [
              'label'=>'Selectionnez votre niveau pour cet exercice.',
              'choices'=>[
                'Débutant'=>'Débutant',
                'Intermédiaire'=> 'Intermédiaire',
                'Expert'=>'Expert',
              ],
            ])
            ->add('commentaire')
            ->add('exercice', EntityType::class, [
              'label'=>'Sélectionnez votre exercice',
              'class'=> Exercice::class,
            ])
            ->add('seance', EntityType::class, [
              'label'=>'Selectionnez vote séance.',
              'class'=> Seance::class,
              'query_builder' => function ( EntityRepository $er ) {
                return $er->createQueryBuilder('s')
                ->where('s.user = :idUser')
                ->setParameter('idUser', $this->tokenStorage->getToken()->getUser()->getId());
              }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Performance::class,
        ]);
    }
}
