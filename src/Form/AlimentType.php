<?php

namespace App\Form;

use App\Entity\Aliment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AlimentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('kcal')
            ->add('glucides')
            ->add('protides')
            ->add('lipides')
            ->add('type', ChoiceType::class, [
              'label'=>'Type de l\'aliment',
              'multiple'=>false,
              'expanded'=>true,
              'choices'=> [
                'Boisson'=>'boisson',
                'Produit laitier'=>'laitier',
                'Viande'=>'viande',
                'Oeuf'=>'oeuf',
                'Poisson'=>'poisson',
                'Créréale'=>'ceréale',
                'Fruit'=>'fruit',
                'Féculent'=>'feculent',
                'Légume'=>'legume',
                'Legumineuse'=>'legumineuse',
                'Autre'=>'autre',
              ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Aliment::class,
        ]);
    }
}
