<?php

namespace App\Form;

use App\Entity\Adresse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class AdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numRue', TextType::class, [
              'label'=> 'Numéro de la rue',
            ])
            ->add('nomRue', TextType::class, [
              'label'=> 'Nom de la rue',
            ])
            ->add('ville', TextType::class, [
              'label'=> 'Nom de la ville',
            ])
            ->add('codePostal', TextType::class, [
              'label'=> 'Code Postal',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adresse::class,
        ]);
    }
}
