<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Adresse;
use App\Form\AdresseType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('email')
            ->add('sexe', ChoiceType::class, [
              'label'=> 'Quel est votre sexe',
              'choices'=> [
                'Homme'=>'Homme',
                'Femme'=>'Femme',
                'Autre'=> 'Autre',
                'Ne souhaite pas renseigner' => 'indéfini',
              ],
            ])
            ->add('niveau', ChoiceType::class, [
              'label' => 'Sélectionnez votre niveau sportif',
              'choices' => [
                'Débutant' => 'Débutant',
                'Intermédiaire'=> 'Intermédiaire',
                'Expert' => 'Expert',
              ],
            ])
            ->add('objectif', ChoiceType::class, [
              'label'=>'Votre objectif',
              'choices'=>[
                'Hypertrophie'=>'Hypertrophie',
                'Gain de force'=> 'Gain de force',
              ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label'=> 'Acceptez les CGU',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les CGU.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'label' => 'Mot de passe',
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Entrez un mot de passe',
                    ]),
                    new Length([
                        'min' => 4,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères.',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('adresse', AdresseType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
