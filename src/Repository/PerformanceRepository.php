<?php

namespace App\Repository;

use App\Entity\Performance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Performance|null find($id, $lockMode = null, $lockVersion = null)
 * @method Performance|null findOneBy(array $criteria, array $orderBy = null)
 * @method Performance[]    findAll()
 * @method Performance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PerformanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Performance::class);
    }

     /**
      * @return Performance[] Returns an array of Performance objects
      */
    public function findPerformanceByUser($userId)
    {
      $entityManager = $this->getEntityManager();

      $query = $entityManager->createQuery(
          'SELECT p
          FROM App\Entity\Performance p
          WHERE p.seance IN (
            SELECT s
            FROM App\Entity\Seance s
            WHERE s.user = :userId
          )'
      )->setParameter('userId', $userId);

      // returns an array of Product objects
      return $query->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Performance
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
