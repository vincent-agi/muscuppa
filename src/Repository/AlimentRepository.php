<?php

namespace App\Repository;

use App\Entity\Aliment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Aliment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Aliment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Aliment[]    findAll()
 * @method Aliment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlimentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Aliment::class);
    }

     /**
      * @return Aliment[] Returns an array of Aliment objects
      */
    public function findAlimentByNom($nomAliment)
    {
      $entityManager = $this->getEntityManager();

      $query = $entityManager->createQuery(
          'SELECT a
          FROM App\Entity\Aliment a
          WHERE a.nom = :nomAliment
          ORDER BY a.nom ASC'
      )->setParameter('nomAliment', $nomAliment);

      // returns an array of Product objects
      return $query->getResult();
    }

    /**
     * @return Aliment[] Returns an array of Aliment objects
     */
   public function sortAlimentByNom()
   {
     $entityManager = $this->getEntityManager();

     $query = $entityManager->createQuery(
         'SELECT a
         FROM App\Entity\Aliment a
         ORDER BY a.nom ASC'
     );

     // returns an array of Product objects
     return $query->getResult();
   }

    /*
    public function findOneBySomeField($value): ?Aliment
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
