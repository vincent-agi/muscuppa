<?php

namespace App\Repository;

use App\Entity\Exercice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Exercice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exercice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exercice[]    findAll()
 * @method Exercice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExerciceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exercice::class);
    }

  /**
    * @return Exercice[] Returns an array of Exercice objects
    */
    public function getExerciceByUser($userId)
    {
      $entityManager = $this->getEntityManager();

      $query = $entityManager->createQuery(
          'SELECT e
          FROM App\Entity\Exercice e
          WHERE e.user = userId
          ORDER BY e.dateAjout ASC'
      )->setParameter('userId', $userId);

      // returns an array of Product objects
      return $query->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Exercice
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
