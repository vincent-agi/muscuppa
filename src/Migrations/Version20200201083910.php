<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200201083910 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE performance ADD exercice_id INT DEFAULT NULL, ADD seance_id INT NOT NULL');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D7968189D40298 FOREIGN KEY (exercice_id) REFERENCES exercice (id)');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D79681E3797A94 FOREIGN KEY (seance_id) REFERENCES seance (id)');
        $this->addSql('CREATE INDEX IDX_82D7968189D40298 ON performance (exercice_id)');
        $this->addSql('CREATE INDEX IDX_82D79681E3797A94 ON performance (seance_id)');
        $this->addSql('ALTER TABLE adresse CHANGE num_rue num_rue INT DEFAULT NULL, CHANGE ville ville VARCHAR(255) DEFAULT NULL, CHANGE code_postal code_postal INT DEFAULT NULL, CHANGE nom_rue nom_rue VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE aliment CHANGE kcal kcal VARCHAR(10) DEFAULT NULL, CHANGE glucides glucides VARCHAR(10) DEFAULT NULL, CHANGE protides protides VARCHAR(10) DEFAULT NULL, CHANGE lipides lipides VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE exercice CHANGE charge charge DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE adresse_id adresse_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', CHANGE date_connexion date_connexion DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adresse CHANGE num_rue num_rue INT DEFAULT NULL, CHANGE ville ville VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE code_postal code_postal INT DEFAULT NULL, CHANGE nom_rue nom_rue VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE aliment CHANGE kcal kcal VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE glucides glucides VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE protides protides VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE lipides lipides VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE exercice CHANGE charge charge DOUBLE PRECISION DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D7968189D40298');
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D79681E3797A94');
        $this->addSql('DROP INDEX IDX_82D7968189D40298 ON performance');
        $this->addSql('DROP INDEX IDX_82D79681E3797A94 ON performance');
        $this->addSql('ALTER TABLE performance DROP exercice_id, DROP seance_id');
        $this->addSql('ALTER TABLE user CHANGE adresse_id adresse_id INT DEFAULT NULL, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`, CHANGE date_connexion date_connexion DATETIME DEFAULT \'NULL\'');
    }
}
