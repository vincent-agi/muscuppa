<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200204170604 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE aliment CHANGE kcal kcal VARCHAR(10) DEFAULT NULL, CHANGE glucides glucides VARCHAR(10) DEFAULT NULL, CHANGE protides protides VARCHAR(10) DEFAULT NULL, CHANGE lipides lipides VARCHAR(10) DEFAULT NULL');
        $this->addSql('ALTER TABLE exercice ADD coef DOUBLE PRECISION DEFAULT NULL, CHANGE charge charge DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE adresse_id adresse_id INT DEFAULT NULL, CHANGE date_connexion date_connexion DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE adresse CHANGE num_rue num_rue INT DEFAULT NULL, CHANGE ville ville VARCHAR(255) DEFAULT NULL, CHANGE code_postal code_postal INT DEFAULT NULL, CHANGE nom_rue nom_rue VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE performance CHANGE exercice_id exercice_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE adresse CHANGE num_rue num_rue INT DEFAULT NULL, CHANGE ville ville VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE code_postal code_postal INT DEFAULT NULL, CHANGE nom_rue nom_rue VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE aliment CHANGE kcal kcal VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE glucides glucides VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE protides protides VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE lipides lipides VARCHAR(10) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE exercice DROP coef, CHANGE charge charge DOUBLE PRECISION DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE performance CHANGE exercice_id exercice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE adresse_id adresse_id INT DEFAULT NULL, CHANGE date_connexion date_connexion DATETIME DEFAULT \'NULL\'');
    }
}
