var namespaceApp =
[
    [ "Controller", "namespaceApp_1_1Controller.html", "namespaceApp_1_1Controller" ],
    [ "DataFixtures", "namespaceApp_1_1DataFixtures.html", "namespaceApp_1_1DataFixtures" ],
    [ "Entity", "namespaceApp_1_1Entity.html", "namespaceApp_1_1Entity" ],
    [ "Form", "namespaceApp_1_1Form.html", "namespaceApp_1_1Form" ],
    [ "Repository", "namespaceApp_1_1Repository.html", "namespaceApp_1_1Repository" ],
    [ "Security", "namespaceApp_1_1Security.html", "namespaceApp_1_1Security" ],
    [ "Kernel", "classApp_1_1Kernel.html", "classApp_1_1Kernel" ]
];