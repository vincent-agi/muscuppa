var classApp_1_1Entity_1_1Seance =
[
    [ "__construct", "classApp_1_1Entity_1_1Seance.html#a370e651a119e1725284d9f82f9d9bf71", null ],
    [ "__toString", "classApp_1_1Entity_1_1Seance.html#a129f4069bf40c8c310654757d31638a8", null ],
    [ "addPerformance", "classApp_1_1Entity_1_1Seance.html#a1cdc20018a8b17a1950d636e3ea24297", null ],
    [ "getDate", "classApp_1_1Entity_1_1Seance.html#a774a77fdbd9e9aa029a685a7bef84b22", null ],
    [ "getId", "classApp_1_1Entity_1_1Seance.html#a15a972bc0620395dc7f00c476a9b587b", null ],
    [ "getPerformances", "classApp_1_1Entity_1_1Seance.html#a5710e5530b9253a7b5ccff06d4bea93a", null ],
    [ "getUser", "classApp_1_1Entity_1_1Seance.html#ab145d8f3b0858bd54bf2db0ff491f4ba", null ],
    [ "removePerformance", "classApp_1_1Entity_1_1Seance.html#abf86cc3614b42e5aa9869a179cb0ba7e", null ],
    [ "setDate", "classApp_1_1Entity_1_1Seance.html#a949dadd829a5b2b7909bfb61fa937791", null ],
    [ "setUser", "classApp_1_1Entity_1_1Seance.html#aef84164ed0cce6e20310e835c5f23b5c", null ],
    [ "$date", "classApp_1_1Entity_1_1Seance.html#af5763c65a91e0c2595a00c6047523b41", null ],
    [ "$id", "classApp_1_1Entity_1_1Seance.html#a6839ecd9cd0349b7db6c5fe565afc3ce", null ],
    [ "$performances", "classApp_1_1Entity_1_1Seance.html#af7ca298aad7405127b89098658bed0c2", null ],
    [ "$user", "classApp_1_1Entity_1_1Seance.html#a9af2dcff1c5f72d9b0dc305f501f988b", null ]
];